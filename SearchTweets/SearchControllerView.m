//
//  SearchControllerView.m
//  SearchTweets
//
//  Created by Kraftly03 on 6/2/17.
//
//

#import "SearchControllerView.h"
#import "STTwitter.h"
#import "SearchTweet.h"
#import "Utils.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"
@interface SearchControllerView ()<UISearchBarDelegate>
{
    NSArray *searchTweetArray;
    AppDelegate *myDelegate;
}
@property (weak, nonatomic) IBOutlet UILabel *noTweetlBL;
@property (weak, nonatomic) IBOutlet UICollectionView *imageCollection;

@property (nonatomic, strong) NSString *stringSearched;
@end

@implementation SearchControllerView
@synthesize twitter;
- (void)viewDidLoad {
    [super viewDidLoad];
    searchTweetArray= [[NSArray alloc]init];
    self.imageCollection.hidden = YES;
    myDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.noTweetlBL.hidden = YES;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - search bar delegates

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    //   WHEN CURSON FOCUSE
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    // WHEN CANCEL BUTTON CLICK
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    self.stringSearched = searchBar.text;
    [searchBar resignFirstResponder];
    [self searchTweets];
}
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
}
#pragma mark - collection view delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return searchTweetArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ImageCell" forIndexPath:indexPath];
    if(searchTweetArray.count>0)
    {
    UIImageView *imgView = (UIImageView *)[cell viewWithTag:34];
    
    NSURL *url = [NSURL URLWithString:[[[searchTweetArray objectAtIndex:indexPath.row] objectForKey:@"user"]objectForKey:@"profile_background_image_url_https"]];
    [imgView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder"] ];
    }
    return cell;
}
#pragma mark - selector

-(void)searchTweets{
      NSString *query = [NSString stringWithFormat:@"#%@",self.stringSearched];
    NSCharacterSet *set = [NSCharacterSet URLHostAllowedCharacterSet];
         NSString *searchQuery = [query stringByAddingPercentEncodingWithAllowedCharacters:set];
            [twitter getSearchTweetsWithQuery:searchQuery geocode:[NSString stringWithFormat:@"%@,%@,10km",myDelegate.latitude,myDelegate.longitude] lang:nil locale:[NSString stringWithFormat:@"%@",[[NSLocale currentLocale]objectForKey:NSLocaleCountryCode]] resultType:@"recent" count:@"10" until:@"2017-06-02" sinceID:nil maxID:nil includeEntities:@(0) callback:nil useExtendedTweetMode:@(1) successBlock:^(NSDictionary *searchMetadata, NSArray *statuses) {
                NSLog(@"%@",statuses);
                searchTweetArray = statuses;
                if(searchTweetArray.count > 0)
                {
                self.imageCollection.hidden = NO;
                [self.imageCollection reloadData];
                }
                else
                {
                    self.noTweetlBL.hidden = NO;
                }
                NSLog(@"%@",searchMetadata);
            } errorBlock:^(NSError *error) {
                NSLog(@"%@",error);
                 self.noTweetlBL.hidden = NO;
            }];
           
           
        }
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
