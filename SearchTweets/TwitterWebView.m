//
//  TwitterWebView.m
//  SearchTweets
//
//  Created by Kraftly03 on 6/2/17.
//
//

#import "TwitterWebView.h"

@interface TwitterWebView ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) id observer;
@end

@implementation TwitterWebView

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSURLRequest *request = [NSURLRequest requestWithURL:_authenticationURL];
    [_webView loadRequest:request];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _observer = [[NSNotificationCenter defaultCenter] addObserverForName:@"TwitterCallback" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        
        NSDictionary *dict = note.userInfo;
        
        NSString *token = dict[@"oauth_token"];
        NSString *verifier = dict[@"oauth_verifier"];
        
        [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"twitterOAuthToken"];
        [[NSUserDefaults standardUserDefaults]  setObject:verifier forKey:@"twitterOAuthVerifier"];
        
        
        [self dismissViewControllerAnimated:YES completion:^{
            
            if (_callBack) {
                _callBack(YES);
            }
            
        }];
    }];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:_observer name:@"TwitterCallback" object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Button Action
- (IBAction)tapCancel:(UIButton *)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        if (_callBack) {
            _callBack(NO);
        }
        
    }];
}


@end
