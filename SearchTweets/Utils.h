//
//  Utils.h
//  MyBarJar
//
//  Created by Brijesh on 30/09/16.
//
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject
+ (void)startLoader;
+ (void)stopLoader;
+ (NSString *)getTruncatedImageURL:(NSString *)string;

@end
