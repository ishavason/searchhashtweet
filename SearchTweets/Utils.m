//
//  Utils.m
//  MyBarJar
//
//  Created by Brijesh on 30/09/16.
//
//

#import "Utils.h"
#import <SVProgressHUD/SVProgressHUD.h>

@implementation Utils

+ (void)startLoader
{
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD show];
    });
}

+ (void)stopLoader
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}

+ (NSInteger)numberOfOccranceOfString:(NSString*)str inString:(NSString *)inString
{
    NSString *string = inString;
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:str options:NSRegularExpressionCaseInsensitive error:&error];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:string options:0 range:NSMakeRange(0, [string length])];
    NSLog(@"Found %d",(int)numberOfMatches);
    
    return numberOfMatches;
}


+ (NSString *)getTruncatedImageURL:(NSString *)string
{
    if ([Utils numberOfOccranceOfString:@"http" inString:string] > 1)
    {
        NSString *s1 = [string stringByReplacingOccurrencesOfString:@"http://52.45.150.254/Barjarapi/UploadImage/" withString:@""];
        
        NSURL *url = [NSURL URLWithString:s1];
        if (!url)
        {
            url = [NSURL URLWithString:string];
        }
        
        return [url absoluteString];
    }
    else if ([string rangeOfString:@"http"].location == NSNotFound)
    {
        return [NSString stringWithFormat:@"http://52.45.150.254/Barjarapi/UploadImage/%@",string];
    }
    return string;
}

@end
