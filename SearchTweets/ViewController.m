//
//  ViewController.m
//  SearchTweets
//
//  Created by Kraftly03 on 6/2/17.
//
//

#import "ViewController.h"
#import "STTwitter.h"
#import "Utils.h"
#import "TwitterWebView.h"
#import "SearchTweet.h"
#import "SearchControllerView.h"
@interface ViewController ()
@property (nonatomic, strong) STTwitterAPI *twitter;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - button click
- (IBAction)loginTwitter:(id)sender {
    [self twitterAuthentication];
}
#pragma mark - Twitter Login Function
- (void)twitterAuthentication
{
    self.twitter = [STTwitterAPI twitterAPIWithOAuthConsumerKey:TwitterConsumerKey
                                                 consumerSecret:TwitterSecretKey];
    
    [_twitter postTokenRequest:^(NSURL *url, NSString *oauthToken) {
        NSLog(@"-- url: %@", url);
        NSLog(@"-- oauthToken: %@", oauthToken);
        
        TwitterWebView *webViewVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TwitterWebView"];
        webViewVC.authenticationURL = url;
        [self presentViewController:webViewVC animated:YES completion:nil];
        
        [webViewVC setCallBack:^(BOOL success) {
            
            if (success) {
                [self fetchUserTwitterInfo];
            }
            
        }];
        
        
    } authenticateInsteadOfAuthorize:NO
                    forceLogin:@(YES)
                    screenName:nil
                 oauthCallback:[NSString stringWithFormat:@"%@://twitter_access_tokens/",TwitterAppName]
                    errorBlock:^(NSError *error) {
                        NSLog(@"-- error: %@", error);
                    }];
}

- (void)fetchUserTwitterInfo
{
    NSString *verifier = [[NSUserDefaults standardUserDefaults] objectForKey:@"twitterOAuthVerifier"];
    
    [Utils startLoader];
    
    [_twitter postAccessTokenRequestWithPIN:verifier successBlock:^(NSString *oauthToken, NSString *oauthTokenSecret, NSString *userID, NSString *screenName) {
        NSLog(@"-- screenName: %@", screenName);
        
        
        
        [_twitter getUsersShowForUserID:userID orScreenName:screenName includeEntities:@(1) successBlock:^(NSDictionary *result) {
            
            [Utils stopLoader];
            ;
            if(result)
            {
                
                SearchControllerView *search = [self.storyboard  instantiateViewControllerWithIdentifier:@"SearchControllerView"];
                search.twitter = _twitter;
                [self.navigationController pushViewController:search animated:YES];     }
            
            
            
        } errorBlock:^(NSError *error) {
            [Utils stopLoader];
            
        }];
        
        
    } errorBlock:^(NSError *error) {
        [Utils stopLoader];
        
        NSLog(@"-- %@", [error localizedDescription]);
    }];
}







@end
