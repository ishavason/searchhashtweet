//
//  SearchControllerView.h
//  SearchTweets
//
//  Created by Kraftly03 on 6/2/17.
//
//

#import <UIKit/UIKit.h>
#import "STTwitter.h"
@interface SearchControllerView : UIViewController
@property (nonatomic, strong) STTwitterAPI *twitter;
@end
