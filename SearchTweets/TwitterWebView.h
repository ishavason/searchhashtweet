//
//  TwitterWebView.h
//  SearchTweets
//
//  Created by Kraftly03 on 6/2/17.
//
//

#import <UIKit/UIKit.h>

@interface TwitterWebView : UIViewController
@property (strong, nonatomic) NSURL *authenticationURL;
@property (strong, nonatomic) void(^callBack)(BOOL success);
@end
